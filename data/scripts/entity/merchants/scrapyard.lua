package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"

local SectorGenerator = include ("SectorGenerator")
local PlanGenerator = include ("plangenerator")

local ConfigLib = include("ConfigLib")
local RegenSYConfigLib = ConfigLib("1861103240")

local AsyncShipGenerator = include ("asyncshipgenerator")


local regenWreckageTimer

local generator
local faction


Scrapyard.initializeRSYParent = Scrapyard.initialize
function Scrapyard.initialize()
	Scrapyard.initializeRSYParent()
	
	regenWreckageTimer = Scrapyard.getNewRegenTimer()
	
	local x, y = Sector():getCoordinates()
	generator = SectorGenerator(x, y)
	faction = Faction()
	
	-- Register callback
	if onServer() then
		if faction and faction.isAIFaction then
			Sector():registerCallback("onRestoredFromDisk", "RegenSYRestoredFromDisk")
		end
	end
end


local RegenSYRestore = Scrapyard.restore
function Scrapyard.restore(data)
	RegenSYRestore(data)
	
    regenWreckageTimer = data.regenWreckageTimer or regenWreckageTimer
end

local RegenSYSecure = Scrapyard.secure
function Scrapyard.secure()
    local data = RegenSYSecure()
    
	data.regenWreckageTimer = regenWreckageTimer

    return data
end


function Scrapyard.RegenSYRestoredFromDisk(timeSinceLastSimulation)
	regenWreckageTimer = regenWreckageTimer - timeSinceLastSimulation
	Scrapyard.updateWreckages()
end


local RegenSYUpdateServer = Scrapyard.updateServer
function Scrapyard.updateServer(timeStep)
	RegenSYUpdateServer(timeStep)
	
	regenWreckageTimer = regenWreckageTimer - timeStep
end


function Scrapyard.updateWreckages()
	RegenSYConfigLib.log(2, "updateWreckages")
	if regenWreckageTimer < 0 then
		
		local resourcesTotal = Scrapyard.getWreckageResources()
		
		local x, y = Sector():getCoordinates()
		local shipVolume = Balancing_GetSectorShipVolume(x, y)
		local initialResources = shipVolume * 1000 -- roughly expected total resources of that scrapyard
		
		local minResources = initialResources / 4 -- When more then 75% of the wreckages are salvaged, generate some new
		
		RegenSYConfigLib.log(3, "Initial resources:", math.floor(initialResources / 1000) / 1000, "mio")
		RegenSYConfigLib.log(3, "Regenerate threshold:", math.floor(minResources / 1000) / 1000, "mio")
		
		if resourcesTotal < minResources then
			Scrapyard.regenerateWreckages()
		end
		
		
		if not Scrapyard.hasDefender() then
			Scrapyard.regenerateDefender()
		end
		
		regenWreckageTimer = Scrapyard.getNewRegenTimer()
	end
end

-- Get total amount of wreckage resources in the sector
function Scrapyard.getWreckageResources()
	local resourcesTotal = 0
	local timer = nil
	if RegenSYConfigLib.getLoglevel() > 2 then
		timer = HighResolutionTimer()
		timer:start()
	end
	
	for _,entity in pairs({Sector():getEntitiesByType(EntityType.Wreckage)}) do
		entity:waitUntilAsyncWorkFinished()
		local resources = Scrapyard.getTotal({entity:getMineableResources()})
		
		if resources == nil or resources <= 10 then
			entity:setPlan(BlockPlan())
		else
			resourcesTotal = resourcesTotal + resources
		end
	end
	
	if RegenSYConfigLib.getLoglevel() > 2 then
		timer:stop()
		RegenSYConfigLib.log(3, "getWreckageResources execution time", timer.milliseconds, "ms")
		timer:reset()
	end
	
	RegenSYConfigLib.log(2, "Total resources", math.floor(resourcesTotal / 1000) / 1000, "mio")
	
	return resourcesTotal
end

-- Check if a defender is present
function Scrapyard.hasDefender()
	defender = false
	for _,entity in pairs({Sector():getEntitiesByFaction(Faction().index)}) do
		if entity.isShip and entity:hasScript("patrol.lua") then
			defender = true
			break
		end
	end
	
	return defender
end

-- Regenerate wreckages
function Scrapyard.regenerateWreckages()
	RegenSYConfigLib.log(3, "Regenerate wreckages")
	
    for i = 0, math.random(150, 250) do
        if math.random() < 0.5 then
            PlanGenerator.makeAsyncShipPlan("createWreckage", nil, faction)
        else
            PlanGenerator.makeAsyncFreighterPlan("createWreckage", nil, faction)
        end
    end
end

function Scrapyard.createWreckage(plan)
	generator:createWreckage(faction, plan)
end

-- Regenerate defender(s)
function Scrapyard.regenerateDefender()
	RegenSYConfigLib.log(3, "Regenerate defender")
	local x, y = Sector():getCoordinates()
	local shipgenerator = AsyncShipGenerator(Scrapyard, nil)
	
	local defenders = math.random(1, 3)
	local faction = Faction()
	
    for i = 1, defenders do
        shipgenerator:createDefender(faction, generator:getPositionInSector())
    end
end


function Scrapyard.getTotal(list)
    local total = 0
    for _,k in pairs(list) do
        total = total + k
    end
    return total
end


function Scrapyard.getNewRegenTimer()
	return RegenSYConfigLib.get("RegenInterval") * 60 * 60 * (math.random() * 0.8 + 0.6) -- Random timer
end
