
meta =
{
    id = "1861103240",
    name = "RegenerativeScrapYards",
    title = "Regenerative Scrap Yards",
    description = "Regenerative Scrap Yards.",
    authors = {"Hammelpilaw"},
    version = "1.0.2",
    dependencies = {
        {id = "Avorion", max = "2.*"},
		{id = "1720259598", min = "0.3.1"}, -- ConfigLib
		--{id = "1722412006", min = "0.0.1"}, -- ExtUtility
    },
    serverSideOnly = true,
    clientSideOnly = false,
    contact = "info@scrap-yard.org",
}
