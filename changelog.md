# Changelog
### 1.0.2
* Updated mod for Avorion 2.*

### 1.0.1
* Updated mod for Avorion 1.0
* Slightly reduced amount of respawned wreckages, to reduce lags

### 1.0
Initial release